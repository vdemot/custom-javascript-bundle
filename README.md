# Custom Javascript Bundle

Creates a custom Javascript package from TypeScript code minified with Google Closure Compiler

---

#### Usage

Start writing code in the ``index.ts`` file.

1. If your code uses imports use the ``build`` or ``build_minified`` command.
2. For simple code you can use compilation by command ``compile``. For simple and advanced minification use command ``compile_min`` or ``compile_min_advanced``.

In development mode, you can debug the code in the browser console. To do this, run the dev command and open [http://0.0.0.0:9000/webpack-dev-server/](http://0.0.0.0:9000/webpack-dev-server/)  in your browser.

All the resulting code will be in the ``dist`` folder.