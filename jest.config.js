module.exports = {
	"transform": {
		"^.+\\.(t|j)sx?$": "ts-jest"
	},
	"testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?|sass)$",
	"moduleFileExtensions": ["ts", "tsx", "js", "jsx"],
	"moduleDirectories": ["node_modules", "ts"],
	"collectCoverageFrom": [
		"ts/**/*.{ts,tsx}",
		"index.ts",
	]
};