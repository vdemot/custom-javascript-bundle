let Path = require('path');
let HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = (env) => {
	let production = env && env['production'] === 'true';

	return  {
		entry: {
			main: Path.resolve(__dirname, 'index.ts'),
		},
		output: {
			path: Path.resolve(__dirname, 'dist'),
			filename: production ? 'index.min.js' : 'index.js'
		},
		devtool: !production ? 'eval-cheap-module-source-map' : false,
		mode: production ? 'production' : 'development',
		performance: {
			hints: false
		},
		stats: {
			children: false,
			reasons: false,
			modules: false
		},
		cache: true,
		devServer: {
			host: '0.0.0.0',
			port: 9000,
			historyApiFallback: true,
			compress: true,
			inline: false,
			stats: {
				hash: true,
				version: true,
				timings: true,
				assets: false,
				chunks: false,
				modules: false,
				reasons: false,
				children: false,
				source: false,
				errors: true,
				errorDetails: true,
				warnings: true
			}
		},
		resolve: {
			modules: ['./', 'node_modules'],
			extensions: ['.js', '.ts']
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env']
						}
					}
				},
				{
					test: /\.ts$/,
					use: [
						{
							loader: 'ts-loader'
						}
					]
				},
			]
		},
		plugins: [
			new HtmlWebpackPlugin({
				template: 'index.html'
			})
		]
	};
};