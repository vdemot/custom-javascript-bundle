import { a } from '../index';

describe('Test example', () => {
	const f = a

	it(`Instance of function`, done => {
		expect(f).toBeInstanceOf(Function);
		done();
	});

	it(`Return arg number`, done => {
		const arg = 1;
		expect(f(arg)).toBe(arg);
		done();
	});

	it(`Return arg string`, done => {
		const arg = '1';
		expect(f(arg)).toBe(arg);
		done();
	});

	it(`Return arg array`, done => {
		const arg = [1, '2', 3];
		expect(f(arg)).toBe(arg);
		done();
	});
});